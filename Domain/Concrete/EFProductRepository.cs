﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete {
    public class EFProductRepository : IProductsRepository {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Product> Products {
            get { return context.Products; }
        }

        public void SaveProduct(Product product) {
            if (product.ProductID == 0) {
                context.Products.Add(product);
            }
            context.SaveChanges();
        }

        public void DeleteProduct(Product product) {
            context.Products.Remove(product);
            context.SaveChanges();
        }
    }
}
